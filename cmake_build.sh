set -e

cd $1
echo "Building CMake project in $PWD..."
cmake .
make